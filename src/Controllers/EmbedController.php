<?php

namespace RSMAddons\StaffEmbed\Controllers;


use RSMAddons\Helpers\StaffHelper;
use SilverStripe\Assets\Image;
use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\View\Requirements;

class EmbedController extends \PageController
{
    public function index(HTTPRequest $request)
    {
        $search = urldecode(trim($request->getVar('s')));
        if ($search == '') {
            return $this->httpError(404);
        }
        $specialism = \Specialisms::get()->filter(['Title' => $search]);
        if ((!$specialism) || ($specialism->count() == 0)) {
            return $this->httpError(404);
        }
        $staff = $specialism->first()->StaffPage();
        if ($staff) {
            $callback = trim($request->getVar('callback'));
            $data = $this->buildDataArray($staff);
            $ct = ($callback ? 'application/javascript' : 'application/json');
            $this->getResponse()->addHeader('Content-Type', $ct);

            return ($callback ? $callback . '(' : '') . json_encode($data) . ($callback ? ')' : '');
        }
        return $this->httpError(404);

    }

    private function buildDataArray($data)
    {
        $staffData = [];
        foreach ($data as $item) {
            $image = (($item->ProfileImageID > 0) && ($item->ProfileImage()->URL != "")) ? Director::absoluteURL($item->ProfileImage()->ScaleWidth(440)->URL) : null;
            $school = ($item->SchoolDetailPageID > 0) ? $item->SchoolDetailPage()->Title : null;
            $staffData[] = [
                "FirstName" => $item->FirstName,
                "Surname" => $item->Surname,
                "Link" => Director::absoluteURL($item->Link()),
                "Image" => $image,
                "JobTitle" => $item->JobTitle,
                "School" => $school
            ];
        }
        return $staffData;
    }
}